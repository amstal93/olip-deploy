===========================
Application developer guide
===========================

This guide aims at describing how to create applications compatible
with the platform and register it on the application catalog.

--------------------
Writing applications
--------------------

The platorm relies on containerization in order to deploy applications
which are isolated from the other ones.

Writing application basically means building docker images.

Once built, images are deployed on ipfs thanks to the
`image2ipfs <https://pypi.org/project/image2ipfs/>`_ python
command and referenced in a json file hosted on ipfs as well.
This file will be called a *descriptor* in the rest of this document.
The descriptor contains the list of docker images to retrieve from IPFS in
order to download and start the application. It contains as well some
important parameters,such as exposed port in order for the application
to be accessible.

---------------------
Deployment descriptor
---------------------

Here is an example of such a descriptor:

.. code-block:: json

  {
    "applications":[
      {
        "bundle": "test.app",
        "name": "testapp",
        "description": "Lorem ipsum dolor sit amet, consectetur",
        "version": "1.0.0",
        "picture": "===BASE64 ENCODED IMAGE ===",
        "containers": [
          {
            "image": "ciqotfvkmfgsbl5yapj6o23v4xjidl2dsci5apykkp5tsssazw4deuq/armhfbuild/nginx:latest",
            "name": "app",
            "expose": 80
          }
        ],
        "contents": [
          {
            "content_id": "test.content",
            "name": "afile",
            "download_path": "/ipfs/Qma6moV1boFTUp4xiEh3WRX8WEYpsKbZXWySnsS6HQ65iJ",
            "destination_path": "path/to/file.pdf",
            "description": "desc",
            "version": "1.0.0",
            "endpoint": {
                "container": "app",
                "name": "Endpoint name",
                "url": "/content/url"
            }
          }
        ]
      },
      {
        "name": "testapp2",
        "bundle": "test.app.2",
        "description": "Lorem ipsum dolor sit amet, consectetur",
        "version": "2.0.0",
        "picture": "===BASE64 ENCODED IMAGE ===",
        "containers": [
          {
            "image": "ciqotfvkmfgsbl5yapj6o23v4xjidl2dsci5apykkp5tsssazw4deuq/armhfbuild/nginx:latest",
            "name": "app",
            "expose": 80
          }
        ],
        "authentication": {
          "grant_types": ["authorization_code", "implicit"],
          "response_types": ["id_token", "code id_token"],
          "token_endpoint_auth_method": "client_secret_post"
        },
        "search": {
          "container": "app",
          "opensearch_url": "/opensearch"
        }
      }
    ],
    "terms": ["term1", "term2"]

  }

In the previous example, two applications are available to the
platform, ``test.app`` and ``test.app.2``.


Here is a description of each fields of the descriptor:

+------------+------------------------------------------------------+
| field      |   description                                        |
+------------+------------------------------------------------------+
| bundle     | System name of the app. Must be                      |
|            | unique over all available applications               |
+------------+------------------------------------------------------+
| name       | This is the display of the app, as it                |
|            | displayed on the home screen                         |
+------------+------------------------------------------------------+
| description| Displayed on the app download screen                 |
|            |                                                      |
+------------+------------------------------------------------------+
| version    | Version of the app. Must be incremented when         |
|            | deploying new functionalities to boxes               |
+------------+------------------------------------------------------+
| picture    | Base 64 encoded png image, which is displayed        |
|            | on the home and catalog screen                       |
+------------+------------------------------------------------------+


The container field is a nested structure that contains the following fields:

+------------+------------------------------------------------------+
| field      |   description                                        |
+------------+------------------------------------------------------+
| image      | Name of the image on ipfs                            |
+------------+------------------------------------------------------+
| name       | Part of the name of the container (see below)        |
+------------+------------------------------------------------------+
| expose     | Exposed port on the host                             |
|            |                                                      |
+------------+------------------------------------------------------+

The contents array allows proposing additional content that may be downloaded
from the administration interface. Each entry in this array match the
following structure:

+-----------------+------------------------------------------------------+
| field           |   description                                        |
+-----------------+------------------------------------------------------+
| content_id      | Id of the content. Must be unique for one            |
|                 | application                                          |
+-----------------+------------------------------------------------------+
| name            | Name of the content. Display in the admin interface  |
+-----------------+------------------------------------------------------+
| version         | Version of the content. Must be incremented when     |
|                 | updating the content                                 |
+-----------------+------------------------------------------------------+
| download_path   | Path on IPFS to retrieve the content from            |
+-----------------+------------------------------------------------------+
| destination_path| Path of the symlink, relative to /data/content       |
+-----------------+------------------------------------------------------+
| description     | Longer description of the content                    |
+-----------------+------------------------------------------------------+

An endpoint can be specified in content, so that it is accessible from
categories including that content. The following attributes can be
specified in the endpoint properties:

+-----------------+------------------------------------------------------+
| field           |   description                                        |
+-----------------+------------------------------------------------------+
| container       | Name of the container on which this content can      |
|                 | be accessed. Must match the name of a declared       |
|                 | container                                            |
+-----------------+------------------------------------------------------+
| name            | Name of the endpoint. Will be displayed in the       |
|                 | interface                                            |
+-----------------+------------------------------------------------------+
| url             | Url on which the content can be accessed, starting   |
|                 | from the root of the context                         |
+-----------------+------------------------------------------------------+

The endpoint url will be deduced from the port the container is listening,
for instance http://X.X.X.X:10001/endpoint/url, where X.X.X.X:10001 is the
ip and port of the container.

The authentication field is a nested object allowing to setup the OIDC
integration if used by the application.

+-----------------------------+-----------------------------------------+
| field                       |   description                           |
+-----------------------------+-----------------------------------------+
| grant_types                 | Type of OIDC flow                       |
+-----------------------------+-----------------------------------------+
| response_types              | Types of response requested by the app  |
+-----------------------------+-----------------------------------------+
| token_endpoint_auth_method  | Authentication mechanism used by the    |
|                             | app on the platform                     |
+-----------------------------+-----------------------------------------+

See the authentication chapter for further details.

The search field is a nested object describing the opensearch interface
that may be exposed by application in order to integrate with the global
search functionality.

+-----------------------------+-----------------------------------------+
| field                       |   description                           |
+-----------------------------+-----------------------------------------+
| container                   | Name of the container. Must match the   |
|                             | description of a container in the app   |
+-----------------------------+-----------------------------------------+
| opensearch_url              | Url, relative to the container root, to |
|                             | the opensearch descriptor               |
+-----------------------------+-----------------------------------------+
| token_endpoint_auth_method  | Authentication mechanism used by the    |
|                             | app on the platform                     |
+-----------------------------+-----------------------------------------+

See implementing the search API for further details

The descriptor to use on the platform is defined by the configuration
variable ``BOX_REPOSITORY_IPN`` in the API config file. The value
of this configuration parameters may be:

* an ipfs address, for instance
  /ipfs/QmacqACcxXgL5ENyFeRPasRR8TtvWLyfMGvhL4tPEnrXA8
* an ipns link, in the form
  /ipns/QmbCrQdshCtrX3vzv87LWVZ7X7iCMZ11DTdtqqLqu4A5Dv
* an ipns link over DNS, in the form /ipns/my.dns.name.org


The terms property allows specifying terms that will form the thesaurus.
These terms can then be associated to categories, playlist, etc.

Maintaining ipns links
______________________

By default, IPFS addresses are hashes of the content they point to.
Therefore, when an updated version of a file is sent on ipfs, the
hash change accordingly. IPNS allows creating links that never
change to IPFS addresses.

In order to publish IPNS, one must create a RSA keypair with the
following command:

  ipfs key gen -t rsa -s 2048 my-key-name

It's then possible to publish an IPNS with the following command:

  ipfs name publish --key=my-key-name
  /ipfs/QmacqACcxXgL5ENyFeRPasRR8TtvWLyfMGvhL4tPEnrXA8

IPNS must be refresh once every 24 hours. It's automatically done
by IPFS if the daemon is alive, but the record will be lost if the
daemon is off. A permanently-on IPFS node must therefore be forseen,
several if high-availability is expected.

The ipfs daemon read the generated key from the ``~/.ipfs/keystore``
folder. Keys can easily be extracted and copied to other nodes in
order to have several publishing nodes.

-----------------
Docker deployment
-----------------

An application may deploy several containers that are described in the
containers field, for instance a database and an application. These
containers will be deployed in a specific network (named with the bundle
of the app). Therefore, containers of the same application may communicate
via their name.

Containers are named by prefixing the ``name`` field from the descriptor
with the bundle of the application. For instance, if a container is named
``c1`` for an application ``test.app``, the final container name will be
``test.app.c1``.

Containers may expose a port on the host via the ``expose`` field. This is
typically used at least on the main container running the application, so that
it is accessible by the user. By using an ``"expose": 80`` directive in the
descriptor, the box will find a random port that respect the range specified
by the ``APPLICATIONS_PORT_RANGE`` settings of the app, and redirect
it on the specified port in the descriptor (for instance 10001 on the host to
80 on the container). The box will then generate links to this application
on the home page (for instance http://localhost:10001).

An application may optionnaly specify the expose directive on several
containers. The box will then generate several links on the home page
that will be suffixed by the container name. For instance, for a container
``c1`` and an app ``test app``,the resulting display name will be ``test
app (c1)``. If only one container uses the expose directive for a single
application, only the app name will be used as display name.

-----------
IPFS images
-----------

IPFS images are downloaded thanks to an IPFS registry deployed on the host
on port 5000. This registry use a specific semantic in order to locate the
image on the network, and the following considerations has to be reminded:

Images are folders deployed on IPFS and have this kind of address:

  ciqotfvkmfgsbl5yapj6o23v4xjidl2dsci5apykkp5tsssazw4deuq/armhfbuild/nginx:latest

The first part of this address is actually the IPFS hash of the folder where
to find the image (encoded differently because image names are case insensitive
on docker).

The "latest" tag must always be specified. Failing to do so will produce an
error when downloading the image. The reason is that, by nature, the hash
always points to a specific image version (the address is the hash of the
content on IPFS). There is therefore no need to use any other tag.

In order to deploy an image on IPFS the image2ipfs tool must before be
installed:

  pip install image2ipfs

In order to effectively expose the image, the following commmand must be
issued:

  docker save armhfbuild/nginx | image2ipfs

The command will display the final address that has to be specified in the
descriptor:

  You can pull using
  localhost:5000/ciqotfvkmfgsbl5yapj6o23v4xjidl2dsci5apykkp5tsssazw4deuq/armhfbuild/nginx

**Note that you must strip the localhost:5000 prefix in the descriptor**. It
will be added by the box itself.

This command save and expose each layer of the image 2 IPFS. The IPFS registry
must be running on the host where the command is used (or on another host if
specified with the ``-r`` parameter of the image2ipfs command).

------------------
Content management
------------------

The platform provides a way to manage optional content directly via the
administration interface. This is an optional feature that may or may not
be used by applications.

The content is downloaded by pinning the content denoted by the download_path
variable of the descriptor. A symlink is then created
in the /data/content folder, the symlink take the name specified in the
destination_path entry of the descriptor. That way of providing content
prevent the space to be consumed twice.

Alternatively, the IPFS fuse filesystems /ipfs and /ipns are bind-
mounted inside each application container. Therefore, application may
choose to use a specific content management system, by still leveraging
IPFS in order to optimise download times.

--------------
Authentication
--------------

The platform integrates an OpenId Connect (OIDC) autorization server in order
for the applications to make users authenticate themselves against the
user-base managed by the platform.

It allows application developers to avoid re-creating each time a user
management feature. Besides, users of the platform may navigate amongst
applications without having to re-authenticate. Of course, application may
choose to provide their own user management mechanism, but users will then
need to re-authenticate via these specific credentials.

OIDC being an additional layer on top of the OAuth 2 protocol, the platform
is able to provide access delegation to applications, i.e. granting access
tokens to applicatons, so that they can use the plaform API in place of the
final user.

A minimal understanding of OAuth and OIDC is therefore reuired. This document
will focus on what the platform provides. For more information, please refer
to the OAuth 2 and OIDC specification.

Grant Types
___________

An OIDC/OAuth authorization server may be used for several use cases and
topology of applications.

This is materialized by different grant types which change the flow of the
authentication. Here is a desription of the grant types the platform
currently supports:

* autorization_code: this grant type is used when there is a backend component
  on the client application (i.e. a standard Web MVC app or a single page app
  with an API). The platform's authorization server emits an authorization
  code, that may be used by the application's backend to retrieve an
  access_token (access delegation) and/or an id token (authentication). No
  access_token is sent to the user agent (browser), increasing the security
  of the solution.

* implicit: in this case, the autorization server directly returns the access
  token/id_token to the user agent. This is especially usefull for pure
  browser applications (i.e. with no backend) where redeeming an
  authorization token would bring no additionaly security value.

The autorization server checks the allowed grant types depending
on what is specified in the application's descriptor in the authentication
section. If your application only requires the autorization_code grant type
(if you have a backend that is able to redeem an authorization code), you
should then remove the implicit grant type from the list in the application
descriptor.

There are other well-known grant types not supported (or not tested) yet:

* password grant: Allows application to directly gather the user's credentials
  (username and password) and redeem a token in exchange. Must only be used on
  trusted applications, therefore not useful for our context.

* client_credentials: allows application to authenticate themselves, i.e.
  without user authentication. May be usefull if application need to perform
  some background job without user interaction. Not impleted yet however.

* refresh_token: Access token may be set to expire. The refresh token is used
  to refresh the access token when it is expired.

Response types
______________

The response types defines what is the result of an authorization request.
There are several response types supported by the platform:

* code: The code response type will request an authorization code from the
  the authorization server. This is the typical response type for the
  authorization_code grant type.

* id_token: The id token is a JWT token that contains a proof of the user's
  identity.

* token: The token response type requests directly an access token from the
  authorization server. This is the typical response type used by the
  implicit grant type.

Client may request a combination of these response types. The authorization
server will then select a flow accordingly: If the code response type is
requested, the authorization server will switch to the authorization code
grant type. If the token response type is selected, we are following the
implicit flow. If a combination of response types is selected, we are in
an "hybrid" case.

The response types that an application may request must be specified in the
response_types entry of the authentication object in the descriptor. The
current list of possibilites is:

* code
* code id_token
* id_token
* token id_token

Endpoint auth method
____________________

In order to secure the application that are allowed to request something
from the authorization server, each application must authenticate against
the authorization server (at least for the authorization_code grant type).

The credential used by appliciations are provided by each container via
environment variable. The following variables are available:

* CLIENT_ID: Id of the client. On this platformn this is the application
  bundle
* CLIENT_SECRET: Password randomly generated by the platform. It is re-
  generated at each installation of the application.
* OIDC_URL: Url of the authorization provider. The authorization server
  supports dynamic discory on the url
  $OIDC_URL/.well-known/oidc-configuration.

Depending on the application, two authentication methods are available:

* client_secret_post: credentials are sent via a POST request to the
  authorization server
* client_secret_basic: credentials are sent via the authorization header

The expected authentication method must be specified in the descriptor
in the token_endpoint_auth_method.

---------------------------
Implementing the search API
---------------------------

The platform provides a global search features, where the user can search
amonst all applications providing the required interface.

The platform itself implements an Opensearch interface. The request is
delegated to other applications, and a single Atom feed si returned.

The path to the opensearch descriptor is /opensearch. It contains the
following URL template for perform queries:
``/opensearch/search?q={searchTerms}``

The received term is forwarded to every application exposing an Opensearch
interface by itself. The url used to request a search from an application
is built from the exposed port from the container. For instance, if the
container is exposed on port 10000, the url triggered by the platform will
be ``http://<platform_ip>:10000/<opensearch_url>``, the opensearch_url beeing
the parameter specified in the application's descriptor under the search
section.

In order to provide absolute URLs, containers deployed via the platform have
access to the ``APPLICATION_ROOT`` environment variable, that specifies the
root url of each container.

