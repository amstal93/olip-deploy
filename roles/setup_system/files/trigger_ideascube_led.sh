#!/bin/bash

max_battery="100"
min_battery="10"
current_state="empty"

function trigger_battery_led()
{
    local state=$1
    echo $state
    current_state=$state

    if [ $state == "charged" ]
    then
        echo default-on | sudo tee /sys/class/leds/led1/trigger
    fi

    if [ $state == "charging" ]
    then
        echo timer | sudo tee /sys/class/leds/led1/trigger
        echo 1000 | sudo tee /sys/class/leds/led1/delay_on
    fi

    if [ $state == "discharging" ]
    then
        echo heartbeat | sudo tee /sys/class/leds/led1/trigger
    fi

    if [ $state == "empty" ]
    then
        echo timer | sudo tee /sys/class/leds/led1/trigger
        echo 200 | sudo tee /sys/class/leds/led1/delay_on
        echo 200 | sudo tee /sys/class/leds/led1/delay_off
    fi
}

while true; do
    battery=$(echo "get battery" | nc -q 0 127.0.0.1 8423 | cut -d ":" -f2)
    battery_power_plugged=$(echo "get battery_power_plugged" | nc -q 0 127.0.0.1 8423 | cut -d ":" -f2 | sed 's/ //')

    if [[ ${battery%.*} -ge $max_battery && $battery_power_plugged == "true" ]]
    then
        state="charged"
    else
        state="discharging"
    fi

    if [[ ${battery%.*} -lt $max_battery && $battery_power_plugged == "true" ]]
    then
        state="charging"
    fi

    if [[ ${battery%.*} -lt $max_battery && ${battery%.*} -gt $min_battery && $battery_power_plugged == "false" ]]
    then
        state="discharging"
    fi

    if [[ ${battery%.*} -lt $min_battery && $battery_power_plugged == "false" ]]
    then
        state="empty"
    fi

    if [ "$current_state" != "$state" ] 
    then
        trigger_battery_led $state
    fi

    sleep 2
done
