---
- name: If external HDD set new OLIP storage directory
  set_fact: 
    olip_storage: "/media/hdd"
  when: ext_hdd

- name: Create a symbolic link for OLIP storage directory
  file: 
    src: /media/hdd/
    dest: /data
    state: link
  when: ext_hdd

- name: Create core network
  become: true
  docker_network:
    docker_host: unix://var/run/balena-engine.sock
    name: "{{olip_network}}"
    driver_options:
      com.docker.network.bridge.name: brolipcore
    ipam_options:
      subnet: "{{olip_core_network.subnet}}"
      gateway: "{{olip_core_network.gateway}}"

- name: Remove previous OLIP container
  become: true
  docker_container:
    docker_host: unix://var/run/balena-engine.sock
    name: "{{item}}"
    state: absent
  loop:
    - olip-api
    - olip-dashboard

- name: Install API
  become: true
  docker_container:
    docker_host: unix://var/run/balena-engine.sock
    name: "{{ olip_api_name }}"
    image: "{{olip_api_repository}}:{{ olip_api_version }}"
    restart_policy: always
    pull: yes
    published_ports:
      - "{{ api_published_ports }}"
    networks:
      - name: "{{olip_network}}"
    networks_cli_compatible: yes
    volumes:
      - "{{ olip_storage }}:{{ olip_storage }}"
      - "{{ docker_host_socket }}:/var/run/balena-engine.sock"
    env:
      DOCKER_HOST: "unix://var/run/balena-engine.sock"
      PROXY_NETWORK: "{{olip_network}}"
      PROXY_ENABLE: "{{ 'true' if proxy_enable | default(omit) == '1' or proxy_enable | default(omit) == 'true' else none}}"
      HOST_DATA_DIR: "{{ olip_storage }}"
      SQLALCHEMY_DATABASE_URI: "sqlite:///{{ olip_storage }}/app.db"
      IPFS_GATEWAY_HOST: "{{ olip_core_network.gateway }}"
      IPFS_GATEWAY_PORT: "{{ olip_ipfs_api_port }}"
      DOCKER_REGISTRY_HOST: "{{ olip_core_network.gateway }}:{{ ipfs_docker_registry_port }}"
      API_BASE_URL: "{{ api_base_url }}"
      APPLICATIONS_ROOT: "{{ applications_root }}"
      INTERNAL_HOST_BASE_URL: "{{olip_internal_host_base_url | default(omit)}}"
      BOX_REPOSITORY_IPN: "{{ olip_file_descriptor }}"
      # BOX_REPOSITORY_IPN: "ipfs:/ipfs/{{ olip_file_descriptor }}"
      OAUTH2_ALWAYS_ACCEPT_REDIRECT_URI: "True"
      OAUTH2_LOGOUT_REDIRECT_URI: "http://{{ olip_external_base_url }}"
      OAUTH2_DEV_MODE: "True"
      APPLICATIONS_PORT_RANGE: "{{ containers_port_range | default(omit) }}"
      UNIQ_ID: "{{ unique_container_id | default(omit) }}"
    labels:
      owner: "{{ docker_label_owner }}"
      port_range_id: "{{ port_range_id | default(omit) }}"
      traefik.enable: "{{proxy_enable | default(omit)}}"
      traefik.http.routers.api.rule: "Host(`api.{{ olip_api_base_url | default(omit) }}`)"
    etc_hosts: "{{ extra_hosts | default(omit) }}"

- name: Install Dashboard
  become: true
  docker_container:
    docker_host: unix://var/run/balena-engine.sock
    name: "{{ olip_dashboard_name }}"
    image: "{{olip_dashboard_repository}}:{{ olip_dashboard_version }}"
    restart_policy: always
    pull: yes
    published_ports:
      - "{{ dash_published_ports }}"
    networks:
      - name: "{{olip_network}}"
    networks_cli_compatible: yes
    env:
      API_URL: "{{api_url}}"
    labels:
      owner: "{{ docker_label_owner }}"
      traefik.enable: "{{proxy_enable | default(omit)}}"
      traefik.http.routers.dashboard.rule: "Host(`{{ olip_api_base_url | default(omit) }}`)"

- name: Copy olip_descriptor_update script
  copy:
    src: olip_descriptor_update.sh
    dest: /usr/local/bin/olip_descriptor_update.sh
    mode: '0755'

- name: Copy systemd timer for olip_descriptor_update
  copy:
    src: "{{ item }}"
    dest: "/etc/systemd/system/{{ item }}"
  loop:
    - olip_descriptor_update.service
    - olip_descriptor_update.timer

- name: Enable and start systemd timer for olip_descriptor_update
  systemd:
    name: olip_descriptor_update.timer
    enabled: yes
    state: started
    masked: no

- name: Remove old cronjob to update the descriptor.json file
  cron:
    name: "Update descriptor"
    job: curl -X GET "http://localhost:5002/applications/?visible=true&repository_update=true"
    state: absent

- name: Alernatively remove old cronjob to update the descriptor.json file
  file: 
    state: absent
    path: /etc/cron.d/descriptor-update

- name: Copy olip_container_upgrade.py script
  template:
    src: "olip_container_upgrade.py.j2"
    dest: /usr/local/bin/olip_container_upgrade.py
    mode: '0755'

- name: Copy systemd timer for olip_container_upgrade
  copy:
    src: "{{ item }}"
    dest: "/etc/systemd/system/{{ item }}"
  loop:
    - olip_container_upgrade.service
    - olip_container_upgrade.timer

- name: Enable and start systemd timer for olip_container_upgrade
  systemd:
    name: olip_container_upgrade.timer
    enabled: yes
    state: started
    masked: no

- name: Remove old cronjob to update OLIP stack
  cron:
    name: "Update OLIP stack"
    cron_file: olip-update-stack
    state: absent

- name: Remove the cronjob from root's crontab
  cron:
    name: "Update descriptor"
    user: root
    state: absent
  tags:
    - hotfixes

- name: Remove old cron job to update container
  file: 
    state: absent
    path: /etc/cron.hourly/update-olip-container
  tags:
    - hotfixes

- name: Remove old script
  file: 
    state: absent
    path: /usr/local/bin/update-olip-container.sh
  tags:
    - hotfixes